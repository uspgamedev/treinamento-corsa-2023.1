# For it to work, obstacle scene needs a Sprite and a CollisionShape2D
extends Node2D

onready var health_component: Health = $Health;
export(int) var obstacle_health = 3;

func _ready():
	health_component.change_max_health(obstacle_health);
	
func _on_Health_dead():
	queue_free();

func _on_HitBox_bullet_enemy_entered(bullet: Bullet_Enemy):
	health_component.take_damage(bullet.damage);
	bullet.queue_free();

func _on_HitBox_bullet_entered(bullet: Bullet):
	health_component.take_damage(bullet.damage);
	bullet.queue_free();

func _on_HitBox_enemy_entered(body):
	self.queue_free();

func _on_HitBox_item_entered(item: Item): # Segurar o item, como?
	pass
