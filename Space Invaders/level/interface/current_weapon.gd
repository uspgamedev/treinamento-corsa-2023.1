extends Node
export var health_size: int = 16
#func _ready():
#	$WeaponCooldown.speed_scale = 15
#	$WeaponSprite.play("wand")

func on_weapon_changed(new_weapon : String):
	$WeaponSprite.set_animation(new_weapon)
	

func on_bullet_fired(delay):
	$WeaponCooldown.frames.set_animation_speed('shoot', 16/(delay/0.25))
	$WeaponCooldown.frame = 0
	$WeaponCooldown.play('shoot')
