extends Node
export var health_size: int = 16
func on_health_changed(health: int):
	$Cheese.rect_size.x = health * health_size
