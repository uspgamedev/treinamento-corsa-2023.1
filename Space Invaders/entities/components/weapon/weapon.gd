class_name Weapon
extends Node

signal bullet_fired();
export var bullet: PackedScene;
export var delay: float;
export var bullet_direction: Vector2;
export var bullet_velocity: float;
onready var timer := $Timer as Timer;
onready var bullet_spawn_point = $"BulletSpawnPoint";
var can_shoot = true;
export var top_level = true;


func _ready():
	timer.wait_time = 0.25 * delay;
	timer.start();
	timer.one_shot = true;

	

func _on_Timer_timeout():
	can_shoot = true;
	


func shoot():
	if can_shoot:
		emit_signal("bullet_fired", delay);
		var new_bullet = bullet.instance();
		add_child(new_bullet);
		new_bullet.set_as_toplevel(top_level);
		new_bullet.global_position = bullet_spawn_point.global_position;
		can_shoot = false;
		timer.wait_time = 0.25 * delay;
		timer.start();
		if ($AnimatedSprite.has_method('special_shoot_animation')):
			$AnimatedSprite.special_shoot_animation()
