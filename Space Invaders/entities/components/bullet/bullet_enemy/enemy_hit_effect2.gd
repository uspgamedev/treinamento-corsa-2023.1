extends Node

var timer = Timer.new()
export var growth_collision = Vector2(3, 2)
onready var collision_node = get_parent().get_node("CollisionShape2D")


func super_effect():
	var new_size_collision = collision_node.shape.get_extents() * growth_collision
	var shape = RectangleShape2D.new()
	shape.set_extents(new_size_collision)
	collision_node.call_deferred("set_shape", shape)
	add_child(timer)
	timer.set_wait_time(0.5)
	timer.set_one_shot(false)
	timer.connect("timeout", self, "delete")
	timer.start()
	get_parent().velocity = 0


func delete():
	get_parent().queue_free()



