extends Node


var timer = Timer.new()
var growth_collision = Vector2(1.0, 1.0)
onready var collision_node = get_parent().get_node("CollisionShape2D")


func _ready():
	pass


func delete():
	get_parent().queue_free()

func super_effect():
	var new_size_collision = collision_node.shape.get_extents() * 2 * growth_collision
	var shape = RectangleShape2D.new()
	shape.set_extents(new_size_collision)
	collision_node.call_deferred("set_shape", shape)
	add_child(timer)
	timer.set_wait_time(0.4)
	timer.set_one_shot(false)
	timer.connect("timeout", self, 'delete')
	timer.start()
	get_parent().velocity = 0


func readyeffect():
	pass
