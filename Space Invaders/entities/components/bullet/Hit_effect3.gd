extends Node
signal play_sfx(sfx);
onready var collision_node = get_parent().get_node("CollisionShape2D")
onready var animation_node = get_parent().get_node("AnimationPlayer")
onready var sprite_node = get_parent().get_node("AnimatedSprite")
export var duration : float = 3;
export var tick_rate: float = 0.25;
var tick_timer = Timer.new()
var able_disable = false
var durations_timer = Timer.new()


func change():

	able_disable = not able_disable
	collision_node.disabled = able_disable
	emit_signal("play_sfx",get_parent().hit_sfx)
	tick_timer.start()

func delete():
	get_parent().queue_free()
	

func readyeffect():
	self.connect("play_sfx",get_tree().root.get_child(1),'_on_sfx_played')
	sprite_node.frames.set_animation_speed('shoot', 3/(tick_rate))
	sprite_node.play('shoot')
	animation_node.play("Effect3")
	collision_node.disabled = true
	add_child(tick_timer)
	tick_timer.set_wait_time(tick_rate)
	tick_timer.set_one_shot(true)
	tick_timer.connect("timeout", self, 'change')
	tick_timer.start()
	add_child(durations_timer)
	durations_timer.set_wait_time(duration)
	durations_timer.set_one_shot(true)
	durations_timer.connect("timeout", self, 'delete')
	durations_timer.start()

func super_effect():
	pass
