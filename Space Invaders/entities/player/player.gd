extends KinematicBody2D
class_name Player

signal game_over()
signal health_changed(health_amount)
signal weapon_changed(weapon_type)
signal play_sfx(sfx);
export var velocity: float = 24000.0;
export var damage_sfx : AudioStreamSample = null;
export var death_sfx : AudioStreamSample = null;
var direction: Vector2;
var movement: Vector2;
onready var current_weapon: Weapon = $Wand;
onready var health_component: Health = $Health;
onready var animator : AnimatedSprite = $AnimatedSprite;
onready var filter : AnimationPlayer = $AnimationPlayer;
var weapon_animator : AnimatedSprite = null;
onready var hitbox = $HitBox;
var isDead = false
export var make_immortal = false;

func _ready():
	self.connect("play_sfx",get_tree().get_root().get_child(1),'_on_sfx_played') # links to Level
	connect("health_changed", get_parent().get_node("InterfaceCanvas/Interface/Life"), "on_health_changed")
	connect("weapon_changed", get_parent().get_node("InterfaceCanvas/Interface/CurrentWeapon"), "on_weapon_changed")
	emit_signal("health_changed", health_component.max_health)
	equip_weapon(current_weapon)
	animator.play('idle');
	weapon_animator.play("idle");

func get_health_component():
	return health_component;


func get_weapon():
	return current_weapon;


func catch_input() -> void:
	direction.x = Input.get_action_strength("move_right") - Input.get_action_strength("move_left");
	if Input.is_action_pressed("shoot"):
		current_weapon.shoot();

func _physics_process(delta: float) -> void:

	
	if (!isDead):
		catch_input();
		
		if direction.length() > 1:
			direction = direction.normalized();
			
		movement.x = direction.x * velocity * delta;
		movement = self.move_and_slide(movement);


func change_weapon(weapon_item : PackedScene):
	self.remove_child(current_weapon)
	var new_weapon = weapon_item.instance() as Weapon
	self.add_child(new_weapon)
	equip_weapon(new_weapon)

func equip_weapon(weapon : Weapon):
	current_weapon = self.get_node(weapon.name)
	weapon_animator = current_weapon.get_node("AnimatedSprite")
	current_weapon.connect("bullet_fired", get_parent().get_node("InterfaceCanvas/Interface/CurrentWeapon"), "on_bullet_fired")
	current_weapon.connect("bullet_fired", self,'_on_bullet_fired')
	emit_signal("weapon_changed", weapon.name)

func _on_Health_damage_taken():
	filter.play('damage')
	emit_signal("play_sfx",damage_sfx)

func _on_Health_heal_taken():
	filter.play('heal')

func dead():
	if (!isDead && !make_immortal):
		isDead = true;
		filter.play('damage')
		emit_signal("play_sfx",death_sfx);
		hitbox.queue_free();
		health_component.queue_free();
		animator.play("death");
		weapon_animator.play("death");
		

func on_enemy_bullet_hit(bullet):
	if(!isDead && !make_immortal):
		health_component.take_damage(bullet.damage);

func _on_animation_finished():
	if (!isDead):
		if (movement.x != 0):
			animator.play('move');
			weapon_animator.play("move");
		else:
			animator.play('idle');
			weapon_animator.play("idle");
	else:
		emit_signal("game_over")

func _on_bullet_fired(_delay):
	animator.play("shoot")
	weapon_animator.play("shoot")


func _on_Health_health_changed(new_health):
	emit_signal("health_changed", new_health)
