extends Node


func apply(target):
	var health = target.get_health_component()
	if is_instance_valid(health):
		health.take_heal(1)
