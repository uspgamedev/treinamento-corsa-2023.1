extends Node


func effect(item_drop):
	if (item_drop == null):
		return
	var drop = item_drop.instance()
	get_tree().get_root().get_child(1).call_deferred("add_child", drop) #add child to level. this should change
	drop.set_as_toplevel(true)
	drop.global_position = self.get_parent().global_position
