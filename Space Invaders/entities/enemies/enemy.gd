extends KinematicBody2D
class_name Enemy
signal enemy_dead(enemy);
onready var weapon: Weapon = $Weapon;
onready var health_component: Health = $Health;
onready var animator: AnimatedSprite = $"AnimatedSprite";

func _ready():
	if weapon.bullet == null:
		weapon.queue_free()
		weapon = null
	
func get_weapon():
	return weapon;

func get_health_component():
	return health_component;

func _on_HitBox_bullet_entered(area):
	health_component.take_damage(area.damage)

func _on_Health_damage_taken():
	$AnimatedSprite/AnimationPlayer.play("damage")

func _on_Health_dead():
	$HitBox.queue_free()
	$CollisionShape2D.queue_free()
	weapon = null
	animator.set_animation("death")
	animator.frame = 0
	animator.play("death")
	emit_signal("enemy_dead", self)
	
func _physics_process(_delta: float) -> void:
	if (weapon != null):
		weapon.shoot()

func _on_bullet_fired():
	animator.play("shoot")

func _on_animation_finished():
	if (animator.animation == 'death' and health_component.get_current_health() < 1):
		self.queue_free()
	var idle_anim = 'idle'
	if (animator.frames.get_animation_names().has('idle2')):
		var random = randi() % 6;
		if (random == 0):
			idle_anim = 'idle2'
	animator.play(idle_anim)

