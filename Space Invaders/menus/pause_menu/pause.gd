extends CanvasLayer


func _input(event):
	if event.is_action_pressed("pause"):
		if not get_tree().paused:
			pause()
		else:
			unpause()


func pause():
	get_tree().paused = true
	show()


func unpause():
	get_tree().paused = false
	hide()


func _on_Pause_pressed():
	get_tree().paused = false
	hide()

func _on_Menu_pressed():
	get_tree().paused = false
	hide()
	get_tree().change_scene("res://menus/main_menu/main_menu_screen.tscn")

func _on_Quit_pressed():
	get_tree().quit()
