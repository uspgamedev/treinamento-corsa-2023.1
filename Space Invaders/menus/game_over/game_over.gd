extends Control

func _on_Restart_pressed():
	hide()
	$Transition.transition_to(Autoload.current_level)


func _on_Quit_pressed():
	get_tree().quit()


func _on_Menu_pressed():
	$Transition.transition_to("res://menus/main_menu/main_menu_screen.tscn")
