extends Node2D
class_name WaveManager

export var followers_number: int = 0;
export var follower_type: Resource;
export var path: PackedScene;
export var path_follow: PackedScene;
export var wave_sec_cooldown: float = 0;
export var followers_velocity: int = 0;
export var loop: bool = 0;
onready var level_manager = get_tree().get_root().get_child(1);
var new_path;
onready var spawn_timer := $Timer as Timer;

export var enable_spawn : bool = true;
var can_spawn: bool = true;

func set_can_spawn (value : bool):
	can_spawn = value;
	if (can_spawn):
		_on_Timer_timeout();
	
func _ready():
	if (can_spawn):
		spawn_timer.start(wave_sec_cooldown);
	new_path = path.instance();
	self.add_child(new_path);

func _on_Timer_timeout():
	can_spawn = enable_spawn and true;

func _spawn(_arg = null):
	if (followers_number > 0):
		if (can_spawn):
			var new_follower = path_follow.instance();
			new_follower.loop = loop
			new_path.add_child(new_follower);
			var new_enemy = follower_type.instance();
			new_follower.add_child(new_enemy);
			new_follower.set_velocity(followers_velocity);
			new_enemy.get_health_component().connect("dead", level_manager, "on_enemy_death");
			followers_number -= 1;
			can_spawn = false;
			if (wave_sec_cooldown):
				spawn_timer.start(wave_sec_cooldown);

func _process(_delta):
	_spawn();

